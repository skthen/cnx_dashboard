/* eslint-disable no-undef */
import React from "react";
import { mount } from "enzyme";
import Table from "../EnhancedTable";
import MomentAdapter from "@date-io/moment";

let wrapped;

beforeEach(() => {
  const moment = new MomentAdapter();

  let headCells = [
    {
      id: "bac",
      label: "BAC",
    },
    {
      id: "vin",
      label: "Vin",
    },
    { id: "ctpStatus", label: "Ctp Status" },
    { id: "onstarStatus", label: "On Star Status" },
    {
      id: "createdAt",
      label: "Created Time",
      display: (timestamp) => {
        let date = new moment.date(timestamp);
        return moment.format(date, "fullDateTime24h");
      },
    },
    {
      id: "color",
      label: "Color",
    },
    {
      id: "stockNumber",
      label: "Stock Number",
    },
    {
      id: "year",
      label: "Year",
    },
  ];

  let rows = [
    {
      _id: "5ba47ea11e867b8c0ac40c9d",
      bac: "122345",
      vin: "VIN00000000000000",
      ctpStatus: "IN-SERVICE",
      onstarStatus: "ONS-116",
      events: [
        {
          _id: "5ba47ea11e867b8c0ac40c9e",
          eventDate: "2018-09-19T14:00:00.000+0000",
          eventType: "created",
        },
      ],
      createdAt: "2018-09-21T05:16:17.927+0000",
      updatedAt: "2018-10-09T02:50:29.624+0000",
      make: "Cadillac",
      model: "T",
      telemetryPnid: "67890",
      color: "Black",
      stockNumber: "12345",
      year: 2018,
    },
  ];

  wrapped = mount(
    <Table
      headers={headCells}
      rows={rows}
      onId={"bac"}
      title={"Vehicles"}
      clickable={false}
    />
  );
});

afterEach(() => {
  wrapped.unmount();
});

it("shows a table", () => {
  expect(wrapped.find("table").length).toEqual(1);
});

it("shows a table header", () => {
  expect(wrapped.find("thead").length).toEqual(1);
});

it("shows a table body (vehicle)", () => {
  expect(wrapped.find("tbody").length).toEqual(1);
});

it("shows the BAC, Vin, Ctp Status, On Star Status, Created Time, Color, Stock Number and Year", () => {
  expect(wrapped.render().text()).toContain("BAC");
  expect(wrapped.render().text()).toContain("Vin");
  expect(wrapped.render().text()).toContain("Ctp Status");
  expect(wrapped.render().text()).toContain("On Star Status");
  expect(wrapped.render().text()).toContain("Created Time");
  expect(wrapped.render().text()).toContain("Color");
  expect(wrapped.render().text()).toContain("Stock Number");
  expect(wrapped.render().text()).toContain("Year");
});

it("shows the Vin number VIN00000000000000", () => {
  expect(wrapped.render().text()).toContain("VIN00000000000000");
});
