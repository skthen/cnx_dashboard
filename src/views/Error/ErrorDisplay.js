import React from "react";
import PropTypes from "prop-types";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const ErrorDisplay = (props) => {
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <GridContainer>
        <GridItem
          xs={12}
          sm={12}
          md={12}
          style={{ display: "flex", justifyContent: "center" }}
        >
          <Typography variant="h2">{props.error.errorName}</Typography>
        </GridItem>
        <GridItem
          xs={12}
          sm={12}
          md={12}
          style={{
            display: "flex",
            justifyContent: "center",
            marginTop: "15px",
          }}
        >
          <span>{props.error.errorDesc}</span>
        </GridItem>
        <GridItem
          xs={12}
          sm={12}
          md={12}
          style={{
            display: "flex",
            justifyContent: "center",
            marginTop: "15px",
          }}
        >
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              props.history.push("/");
            }}
          >
            Return to Dashboard
          </Button>
        </GridItem>
      </GridContainer>
    </div>
  );
};

ErrorDisplay.propTypes = {
  error: PropTypes.object,
  errorName: PropTypes.string,
  errorDesc: PropTypes.string,
  history: PropTypes.object,
};

export default ErrorDisplay;
