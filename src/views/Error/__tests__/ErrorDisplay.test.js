/* eslint-disable no-undef */
import React from "react";
import { mount } from "enzyme";
import Error from "../ErrorDisplay";

let wrapped;

beforeEach(() => {
  let props = {
    history: { length: 50, action: "POP" },
  };

  wrapped = mount(
    <Error
      {...props}
      error={{
        errorName: "Error!",
        errorDesc:
          "Error encountered, click button below to return to Dashboard...",
      }}
    />
  );
});

afterEach(() => {
  wrapped.unmount();
});

it("shows a GridContainer", () => {
  expect(wrapped.find("GridContainer").length).toEqual(1);
});

it("shows three GridItem", () => {
  expect(wrapped.find("GridItem").length).toEqual(3);
});

it("shows the Error! and Error description text", () => {
  expect(wrapped.render().text()).toContain("Error!");
  expect(wrapped.render().text()).toContain(
    "Error encountered, click button below to return to Dashboard..."
  );
});
