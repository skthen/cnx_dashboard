/* eslint-disable no-undef */
import React from "react";
import { mount } from "enzyme";
import Dealer from "../Dealer";

let wrapped;

beforeEach(() => {
  wrapped = mount(<Dealer />);
});

afterEach(() => {
  wrapped.unmount();
});

it("has either Loading, Typography or a Table", () => {
  expect(
    wrapped.find("Loading").length ||
      wrapped.find("Typography").length ||
      wrapped.find("Table").length
  ).toEqual(1);
});
