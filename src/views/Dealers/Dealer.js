import React, { useState, useEffect } from "react";
import Table from "../../components/Table/EnhancedTable";
import dealer from "../../apis/dealer.js";
import Loading from "../Loading/Loading.js";
import Error from "../Error/ErrorDisplay.js";

const headCells = [
  {
    id: "bac",
    label: "BAC",
  },
  {
    id: "name",
    label: "Name",
  },
  { id: "city", label: "City" },
  { id: "state", label: "State" },
  {
    id: "country",
    label: "Country",
  },
  {
    id: "brand",
    label: "Cadillac",
  },
];

export default function Dealer(props) {
  const [dataOk, setDataOk] = useState(null);
  const [rows, setRows] = useState([]);

  useEffect(() => {
    getDealers();
  }, []);

  const getDealers = async () => {
    try {
      const response = await dealer.get("/dealers");
      setDataOk(true);
      setRows(response.data);
    } catch ({ response }) {
      setDataOk(false);
      if (response) {
        setRows([response.status, response.data.message]);
      }
    }
  };

  const renderContent = () => {
    if (dataOk === null) {
      return <Loading />;
    }

    if (!dataOk) {
      if (rows[0] === 500) {
        return (
          <Error
            {...props}
            error={{ errorName: rows[0], errorDesc: rows[1] }}
          />
        );
      } else {
        return (
          <Error
            {...props}
            error={{
              errorName: "Error!",
              errorDesc:
                "Error encountered, click button below to return to Dashboard...",
            }}
          />
        );
      }
    }

    return (
      <React.Fragment>
        <Table
          headers={headCells}
          rows={rows}
          onId={"bac"}
          title={"Dealers"}
          clickable={true}
        />
      </React.Fragment>
    );
  };

  return renderContent();
}
