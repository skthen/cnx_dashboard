import React from "react";
import Dealer from "../Dealers/Dealer.js";

export default function Dashboard(props) {
  return (
    <div>
      <Dealer {...props} />
    </div>
  );
}
