/* eslint-disable no-undef */
import React from "react";
import { shallow } from "enzyme";
import Dashboard from "../Dashboard";
import Dealer from "../../Dealers/Dealer";

let wrapped;

beforeEach(() => {
  wrapped = shallow(<Dashboard />);
});

it("shows a Dashboard", () => {
  expect(wrapped.find(Dealer).length).toEqual(1);
});
