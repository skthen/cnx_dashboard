import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Table from "../../components/Table/EnhancedTable";
import dealer from "../../apis/dealer.js";
import Loading from "../Loading/Loading.js";
import Error from "../Error/ErrorDisplay.js";
import MomentAdapter from "@date-io/moment";

const moment = new MomentAdapter();

const headCells = [
  {
    id: "bac",
    label: "BAC",
  },
  {
    id: "vin",
    label: "Vin",
  },
  { id: "ctpStatus", label: "Ctp Status" },
  { id: "onstarStatus", label: "On Star Status" },
  {
    id: "createdAt",
    label: "Created Time",
    display: (timestamp) => {
      let date = new moment.date(timestamp);
      return moment.format(date, "fullDateTime24h");
    },
  },
  {
    id: "color",
    label: "Color",
  },
  {
    id: "stockNumber",
    label: "Stock Number",
  },
  {
    id: "year",
    label: "Year",
  },
];

const Vehicle = (props) => {
  const [dataOk, setDataOk] = useState(null);
  const [vehicles, setVehicles] = useState([]);

  useEffect(() => {
    getVehicles(props.match.params.id);
  }, []);

  const getVehicles = async (bac) => {
    try {
      const response = await dealer.get(`/vehicles/${bac}`);
      setDataOk(true);
      setVehicles([response.data]);
    } catch ({ response }) {
      setDataOk(false);
      if (response) {
        setVehicles([response.status, response.data.message]);
      }
    }
  };

  const renderContent = () => {
    if (dataOk === null) {
      return <Loading />;
    }

    if (!dataOk) {
      if (vehicles[0] === 500) {
        return (
          <Error
            {...props}
            error={{ errorName: vehicles[0], errorDesc: vehicles[1] }}
          />
        );
      } else {
        return (
          <Error
            {...props}
            error={{
              errorName: "Error!",
              errorDesc:
                "Error encountered, click button below to return to Dashboard...",
            }}
          />
        );
      }
    }

    return (
      <React.Fragment>
        <Table
          headers={headCells}
          rows={vehicles}
          onId={"bac"}
          title={"Vehicles"}
          clickable={false}
        />
      </React.Fragment>
    );
  };

  return renderContent();
};

Vehicle.propTypes = {
  props: PropTypes.object,
};

export default Vehicle;
