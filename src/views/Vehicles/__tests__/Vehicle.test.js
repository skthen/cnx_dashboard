/* eslint-disable no-undef */
import React from "react";
import { mount } from "enzyme";
import Vehicle from "../Vehicle";

let wrapped;

beforeEach(() => {
  let props = {
    history: { length: 50, action: "POP" },
    location: {
      pathname: "/vehicles/122345",
      search: "",
      hash: "",
      state: undefined,
      key: "l8c2we",
    },
    match: {
      path: "/vehicles/:id",
      url: "/vehicles/122345",
      isExact: true,
      params: { id: "122345" },
    },
    staticContext: undefined,
  };

  wrapped = mount(<Vehicle {...props} />);
});

afterEach(() => {
  wrapped.unmount();
});

it("has either Loading, Typography or a Table", () => {
  expect(
    wrapped.find("Loading").length ||
      wrapped.find("Typography").length ||
      wrapped.find("Table").length
  ).toEqual(1);
});
