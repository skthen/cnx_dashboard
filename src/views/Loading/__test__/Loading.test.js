/* eslint-disable no-undef */
import React from "react";
import { shallow } from "enzyme";
import Loading from "../Loading";
import CircularProgress from "@material-ui/core/CircularProgress";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";

let wrapped;

beforeEach(() => {
  wrapped = shallow(<Loading />);
});

it("shows a CircularProgress animation", () => {
  expect(wrapped.find(CircularProgress).length).toEqual(1);
});

it("shows a GridContainer", () => {
  expect(wrapped.find(GridContainer).length).toEqual(1);
});

it("shows two GridItem", () => {
  expect(wrapped.find(GridItem).length).toEqual(2);
});
