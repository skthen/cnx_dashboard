import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";

export default function Loading() {
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <GridContainer>
        <GridItem
          xs={12}
          sm={12}
          md={12}
          style={{ display: "flex", justifyContent: "center" }}
        >
          <CircularProgress />
        </GridItem>
        <GridItem
          xs={12}
          sm={12}
          md={12}
          style={{
            display: "flex",
            justifyContent: "center",
            marginTop: "15px",
          }}
        >
          <span>Loading...please wait</span>
        </GridItem>
      </GridContainer>
    </div>
  );
}
