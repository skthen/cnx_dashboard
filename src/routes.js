// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import Vehicle from "views/Vehicles/Vehicle.js";

const dashboardRoutes = [
  {
    path: "/dealers",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin",
    show: true,
  },
  {
    path: "/vehicles/:id",
    name: "View Vehicle",
    icon: "content_paste",
    component: Vehicle,
    layout: "/admin",
    show: false,
  },
];

export default dashboardRoutes;
