# [Cnx Dashboard](http://cnxdashboard.s3-website-ap-southeast-2.amazonaws.com/)

## Quick start

Quick start options:

- Clone the repo: `git clone https://skthen@bitbucket.org/skthen/cnx_dashboard.git`.
- `npm install`
- `npm start`

## Testing details

- If on first run, run `npm install` to install dependencies
- `npm run test`

## Hosting details

- Frontend code hosted on AWS S3 Bucket via Static website hosting http://cnxdashboard.s3-website-ap-southeast-2.amazonaws.com

- API endpoints hosted on AWS API Gateway. It calls the provide API endpoints with Access-Control-Allow-Origin set to http://cnxdashboard.s3-website-ap-southeast-2.amazonaws.com
  - Dealers: https://d1uymj4f5f.execute-api.ap-southeast-2.amazonaws.com/dev/dealers
  - Vehicle: https://d1uymj4f5f.execute-api.ap-southeast-2.amazonaws.com/dev/vehicles/{id} E.g.: https://d1uymj4f5f.execute-api.ap-southeast-2.amazonaws.com/dev/vehicles/122345

## Licensing

- Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/main/LICENSE.md)
